import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Logout from './Logout'
import { logoutUser } from '../actions'

export default class Navbar extends Component {

  render() {
    const { dispatch, isAuthenticated } = this.props

    return (
      <nav className="navbar has-shadow">
        <div className="navbar-brand">
          <div className="navbar-item">
            <strong>React Matermost Chat </strong>
          </div>
          <span className="navbar-burger">
            <span />
            <span />
            <span />
          </span>
        </div>
        <div className="navbar-menu">
          <div className="navbar-end">
            {isAuthenticated &&
              <Logout onLogoutClick={() => dispatch(logoutUser())} />
            }
          </div>
        </div>
      </nav>
    )
  }

}

Navbar.propTypes = {
  dispatch: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool.isRequired
}

import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class Chat extends Component {

  render() {
    const { isAuthenticated } = this.props

    return (
      <div>
        { isAuthenticated &&
          <p>chat</p>
        }
      </div>
    )
  }
}

Chat.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
}

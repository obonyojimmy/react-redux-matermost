import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class Login extends Component {

  handleClick() {
    const username = this.refs.username
    const password = this.refs.password
    const creds = { username: username.value.trim(), password: password.value.trim() }
    this.props.onLoginClick(creds)
  }

  render() {
    const { errorMessage } = this.props

    return (
      <section className="hero is-medium">
        <div className="hero-body">
          <div className="container">
            <div className="columns is-mobile">
                <div className="column is-half is-offset-one-quarter">
                    {errorMessage &&
                      <div className="notification is-warning">{errorMessage}</div>
                    }

                    <div className="field">
                      <label className="label">Username</label>
                      <div className="control">
                        <input ref="username" className="input" type="text" placeholder="Username" />
                      </div>
                    </div>
                    <div className="field">
                      <label className="label">Password</label>
                      <div className="control">
                        <input ref="password" className="input" type="password" placeholder="Password" />
                      </div>
                    </div>
                    <div className="field has-addons has-addons-centered">
                      <div className="control">
                        <button onClick={(event) => this.handleClick(event)} className="button is-primary">Login</button>
                      </div>
                    </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }

}

Login.propTypes = {
  onLoginClick: PropTypes.func.isRequired,
  errorMessage: PropTypes.string
}

import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { loginUser } from '../actions'
import Login from '../components/Login'
import Navbar from '../components/Navbar'
import Chat from '../components/Chat'

class App extends Component {

  render() {
    const { dispatch, isAuthenticated, errorMessage } = this.props
    return (
      <div>
        <Navbar
          isAuthenticated={isAuthenticated}
          errorMessage={errorMessage}
          dispatch={dispatch}
        />

        {!isAuthenticated &&
          <Login
            errorMessage={errorMessage}
            onLoginClick={ creds => dispatch(loginUser(creds)) }
          />
        }

        {isAuthenticated &&
          <Chat
            isAuthenticated={isAuthenticated}
          />
        }

      </div>
    )
  }
}

App.propTypes = {
  dispatch: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  errorMessage: PropTypes.string
}

function mapStateToProps(state) {
  const {  auth } = state
  const { isAuthenticated, errorMessage } = auth

  return {
    isAuthenticated,
    errorMessage
  }
}

export default connect(mapStateToProps)(App)

